var DB = {
  teams: [
    {
      id: 1,
      name: "Athletico",
      state: "Paraná",
      city: "Curitiba",
      division: "A",
      titles: { international: 2, national: 4, state: 26 },
      payroll: 33555000,
    },
    {
      id: 2,
      name: "Coritiba",
      state: "Paraná",
      city: "Curitiba",
      division: "B",
      titles: { international: 0, national: 4, state: 38 },
      payroll: 1120500,
    },
    {
      id: 3,
      name: "Paraná Clube",
      state: "Paraná",
      city: "Curitiba",
      division: "C",
      titles: { international: 0, national: 2, state: 7 },
      payroll: 187001,
    },
    // {

    //   "name": "Flamengo",
    //   "state": "Rio de Janeiro",
    //   "city": "Rio de Janeiro",
    //   "division": "A",
    //   "titles": { "international": 6, "national": 14, "state": 37 },
    //   "payroll": 98523001
    // }
  ],
};

module.exports = DB;
