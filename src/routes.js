const { json } = require("express");
const express = require("express");
const routes = express.Router();
var DB = require("./teams");

//POST NEW TEAM
routes.post("/newTeam", (req, res) => {
  const { name, state, city, division, titles, payroll } = req.body;

  if (name == undefined || name == "") {
    res.status(400).json({ msg: "Nome é um campo Obrigatório" });
  } else if (state == undefined || state == "") {
    res.status(400).json({ msg: "Estado é um campo Obrigatório" });
  } else if (city == undefined || city == "") {
    res.status(400).json({ msg: "Cidade é um campo Obrigatório" });
  } else if (titles == undefined || titles == "") {
    res
      .status(400)
      .json({ msg: "Quantidade de títulos são campos Obrigatórios" });
  } else if (payroll == undefined || payroll == "") {
    res.status(400).json({ msg: "A folha de pagamento é um dado Obrigatório" });
  } else if (
    division != "A" &&
    division != "B" &&
    division != "C" &&
    division == undefined
  ) {
    res
      .status(400)
      .json({ msg: "Um time pode participar apenas das divisões A, B ou C" });
  } else {
    var aux = DB.teams[DB.teams.length - 1]; // pega o ultimo time
    var id = aux.id + 1;
    DB.teams.push({
      id,
      name,
      state,
      city,
      division,
      titles,
      payroll,
    });
    res.status(200).json({ msg: "Time incluido com sucesso" });
  }
});

//GET ALL TEAMS
routes.get("/teams", (req, res) => {
  if (!DB.teams) res.status(404).json({ msg: "Nenhum time cadastrado" });
  else res.status(200).json(DB.teams);
});

//GET TEAMS BY NAME
routes.post("/teams", (req, res) => {
  const name = req.body.name; //Pega o nome da requisição
  let resposta = []; // definindo a variável de resposta a requisição
  for (var i = 0; i < DB.teams.length; i++) {
    //for para varrer os nomes buscando pela string escrita.
    var time = DB.teams[i]; //pegando o id pra poder comparar o nome.
    var encontrado = time.name.indexOf(name); //A funçã indexOf indica em qual posição ela encontrou a string.
    if (encontrado >= 0) {
      // Se ela não encontra a string na frase, o valor atribuido é -1.
      resposta.push(time); //insere na resposta o time encontrado
    }
  }
  if (resposta.length == 0) {
    //se tiver sido inserido algum time o tamanho vai ser maior que 0
    resposta = "Time não encontrado"; // se não então não encontrou
  }
  res.json({ msg: resposta }); //manda a resposta.
});

//POST UPDATE TEAM
routes.put("/teams/:id", (req, res) => {
  if (isNaN(req.params.id)) {
    //id != numero
    res.sendStatus(400).json({ msg: "O valor do id não é um numero" });
  } else {
    const id = parseInt(req.params.id);
    const team = DB.teams.find((c) => c.id == id);
    if (team != undefined) {
      const { name, state, city, division, titles, payroll } = req.body;
      if (name != undefined && name != "") team.name = name;
      if (state != undefined && state != "") team.state = state;
      if (city != undefined && city != "") team.city = city;
      if (division != undefined) team.division = division;
      if (titles != undefined && titles != "") team.titles = titles;
      if (payroll != undefined && payroll != "") team.payroll = payroll;

      res.status(200).json(team);
    } else {
      res.status(404).json({ msg: "Time não encontrado" });
    }
  }
});

//POST DELETE TEAM
routes.delete("/teams/:id", (req, res) => {
  if (isNaN(req.params.id)) {
    //id != numero
    res.sendStatus(400).json({ msg: "O valor do id não é um numero" });
  } else {
    const id = parseInt(req.params.id);
    const index = DB.teams.findIndex((c) => c.id == id);
    if (index === -1) {
      res.status(404).json({ msg: "Time não existe" });
    } else {
      DB.teams.splice(index, 1);
      res.status(200).json({ msg: "Time removido" });
    }
  }
});

module.exports = routes;
